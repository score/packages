#!/bin/sh

# if prim = nothing exit
prim="$(xclip -o)"; [ -z "$prim" ] && exit

PID=$(xprop -id "$(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}')" | grep -m 1 PID | cut -d " " -f 3)

PID=$(echo "$(pstree -lpA "$PID" | tail -n 1)" | awk -F'---' '{print $NF}' | sed -re 's/[^0-9]//g')

cd "$(readlink /rpoc/"$PID"/cwd)"

[ -f "$prim" ] && xdg-open "$prim" && exit

[ -d "$prim" ] && "$TERMINAL" "$prim" && exit

websearch() { "$BROWSER" "https://duckduckgo.com/?q=$@" ;}

echo "$prim" | grep "^.*\.[A-Za-z]\+.*" >/dev/null && gotourl() { "$BROWSER" "$@" ;}

func="$(declare -F | awk '{print $3}' | dmenu -p "Plumb $prim to?" -i -l 5)"

[ -z "$func" ] || "$func" "$prim"
