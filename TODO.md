# TODO List for Puffles' Packages

This is the todo list for Puffles' Packages.

**NOTE: "-" indicate that the objective has been achived, but is in need of an audit. Everything recives a constant audit, however "-" are used for those which have recived no audit**

#### Net Packages

These are packages for network related tasks.

##### Connect to Network Script

Synopsis:

write a script to connect to networks manually using wpa_supplicant and dhclient

Functionality of Script:

* list available network interfaces -

* ask which interface the user would like to use -

* check if the interface is for enthernet -

* check if the selected interface is up or down, if down set the interface up -

* scan for available networks -

* ask which network the user would like to use -

* if the interface being used is Ethernet, then connect to the network selected -

* check if the network is open or requires a password

* if the network is open connect, else if the network is WPA (closed) then ask for the password

* associate the interface with desired network -

* connect to the network -

* if the script is run with the `-s` option, then save the network information in wpa_supplicant -

* save interfaces to an array so you can just put the number instead of typing the entire INET

* save ssids to an array so you can just put the number instead of typing the entire SSID

##### Crack Network

Synopsis:

write a script to crack networks using aircrack-ng

Functionality of Script:

* put wifi adpater in monitor mode -

* dump for a specific amount of time, then kill the dump

* ask the uer if they want to crack a specific network

* if the user says yes, then list the SSIDs found

* save the SSID the user picked

* get the channel of the picked network

* get the bssid of the picked network

* dump on the picked network in the background (make sure this is killed after the crack is done)

* get the station of the network

* attempt to deauthorize the network using the station, if that fails use the bssid

* save the cap to /tmp/hack (number).cap

* check if the handshake has been found

* once the handshake has been found, use the crack-net word list to attempt to crack the network

* add the above for all network types

* if the -a2 option is used crack for wpa2

* if the -a option is used crack for wpa

* if the -e option is used crack for wep

* if the -w option is used then crack open network by using the macadress of an already authorized user (used to bypass login portal)

* if the user says no to cracking a specific network, then try to crack all networks found until you either have connected to one or have tried to against all

#### Util Packages

These are packages for core utility type tasks.

##### Xfile Script:

Synopsis:

write a file browser using dmenu

Functionality of Script:

* list all files and directories in the current working directory

* let the user select something

* if the user selects a directory cd into it and repeat

* if the user selects a file, then figure out what file type it is and do whatever with it:

* if file type is text or code open in $EDITOR

* give commands with `!command`

* commands to add: unzip, untar, ungzip, zip, tar, gzip, move, rename, remove, oterm, obrowser

##### Bleach Script:

Synopsis:

write a package to wipe all data and deadspace in specific directories (such as cache)

#### Plumber Script:

Synopsis:

write a package to act as a UNIX plumber

#### VM Script:

Synopsis:

write a package to run VMs

Functionality of Script:

* check for already created vms

* show them to the user

* run whatever vm the user picked

* create a new VM

* create a new disposable VM

#### Screen Record Script:

Synopsis:

write a package to record the screen for tty/x

#### Entertainment Packages

These are packages for entertainment related tasks.

#### Secure Run Packages

These are packages for running packages with more secure options.
