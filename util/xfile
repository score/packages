#!/bin/sh

# global variables
option=1
extra_actions='uzip utar ugzip czip ctar cgzip rename remove oterm obrowser'

# list directories then everything else
prompt_for_file() {
	picked=$({
			echo ..
			find . -mindepth 1 -maxdepth 1 -type d -print
			find . -mindepth 1 -maxdepth 1 ! -type d -print
			printf '!%s\n' $2
		} | cut -d/ -f2 | dmenu -i -p "$1:")
}

# determine file-type
get_type() {
	[ -d "$picked" ] && { type=directory; return; }
	[ -p "$picked" ] && { type=fifo; return; }
	[ -b "$picked" ] && { type=block; return; }
	[ -c "$picked" ] && { type=character; return; }
	[ -S "$picked" ] && { type=socket; return; }
	type=$(file - <"$picked" | cut -d' ' -f2)
}

# commands
uzip() {
	prompt_for_file uzip || return
	unzip "$picked" > /dev/null 2>&1
}

utar() {
	prompt_for_file utar || return
	tar -xvf "$picked" > /dev/null 2>&1
}

ugzip() {
	prompt_for_file ugzip || return
	gzip -d "$picked"
}

czip() {
	prompt_for_file czip || return
	zname=$(dmenu -i -p "zip name:" </dev/null)

	zip "$zname" "$picked" > /dev/null 2>&1
}

ctar() {
	prompt_for_file ctar || return
	tname=$(dmenu -i -p "tar name:" </dev/null)

	tar -cf "$tname" "$picked" > /dev/null 2>&1
}

cgzip() {
	prompt_for_file cgzip || return
	gname=$(dmenu -i -p "gzip name:" </dev/null)

	tar -czf "$gname" "$picked" > /dev/null 2>&1
}

rename() {
	prompt_for_file rename || return
	new_name=$(dmenu -i -p "new name:" </dev/null)

	mv "$picked" "$new_name" > /dev/null 2>&1
}

remove() {
	prompt_for_file remove || return
	answer=$(dmenu -i -p "remove $picked (yes/no):" </dev/null)

	if [ "$answer" = "yes" ]; then
		rm "$picked" > /dev/null 2>&1

	else
		exit 1 > /dev/null 2>&1

	fi
}

oterm() {
	prompt_for_file oterm || return
	$TERMINAL "$picked" > /dev/null 2>&1
}

obrowser() {
	prompt_for_file obrowser || return
	$BROWSER "$picked" > /dev/null 2>&1
}

# determine action
action() {
	if [ "$type" = "directory" ]; then
		cd "$picked"

	elif [ "$type" = "bourne" ] || [ "$type" = "ASCII" ] || [ "$type" = "empty" ];  then
		$TERMINAL $EDITOR "$picked" && option=2

	elif [ "${picked#!}" != "$picked" ]; then
		eval "${picked#!}" && option=2

	else
		exit 1

	fi
}

while [ "$option" != 2  ]; do
	prompt_for_file xfile "$extra_actions" || break
	get_type
	action
done
